const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


//Router for Display of All Active Products
router.get("/",productControllers.displayProduct);

//Router for Display of All Active Products
router.get("/singleProduct/:productId",productControllers.showSingleProduct);


//Router for creating products
router.post('/addProduct',verify,verifyAdmin,productControllers.addProduct);

//Router for Updating Product
router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

//Router for Archiving Products
router.delete('/archiveProduct/:productId',verify,verifyAdmin,productControllers.archiveProduct);


//Router for Activating Order
router.put("/activateProduct/:productId",verify,verifyAdmin,productControllers.activateProduct);

//Router for Display of All Active Products
router.get("/getAllProducts",verify,verifyAdmin,productControllers.displayAllProduct);

//Router for deleting
router.delete("/deleteProduct/:productId",verify,verifyAdmin,productControllers.deleteProduct)

module.exports = router;


